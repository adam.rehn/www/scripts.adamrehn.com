#!/usr/bin/env python3
import argparse, yaml, sys
from natsort import natsorted
from natsort.ns_enum import ns


# Parses a YAML file
def read_yaml_file(filename):
	with open(filename, 'r') as f:
		return yaml.load(f, Loader=yaml.SafeLoader)

# Formats a YAML file
def write_yaml_file(filename, data):
	
	# Lambda for writing YAML data with our preferred formatting options
	write = lambda stream: yaml.dump(
		data,
		stream,
		Dumper=yaml.SafeDumper,
		sort_keys=False,
		width=999999999
	)
	
	# If no filename was specified then write to stdout
	if filename is None:
		return write(sys.stdout)
	
	# Otherwise, write to the specified output file
	with open(filename, 'w') as f:
		return write(f)


# Parse our command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument('first', help='The path of the first YAML manifest file')
parser.add_argument('second', help='The path of the second YAML manifest file')
parser.add_argument('--outfile', default=None, help='The output file (defaults to stdout)')
args = parser.parse_args()

# Parse the two YAML manifest files
first = read_yaml_file(args.first)
second = read_yaml_file(args.second)

# Extract the list of unique keys from the two manifests, maintaining the order they are first encountered
keys = list({k:k for k in list(first.keys()) + list(second.keys())}.keys())

# Merge the values for each key, sorting them alphabetically whilst ignoring case
merged = {
	key: natsorted(first.get(key, []) + second.get(key, []), alg=ns.IGNORECASE)
	for key in keys
}

# Write the merged data to the output file (or stdout if no output filename was specified)
write_yaml_file(args.outfile, merged)
