# Download the latest version of `Convert-WindowsImage.psm1` from the Microsoft virtualisation tools repository
Set-ExecutionPolicy Bypass -Scope Process -Force;
iex ((New-Object System.Net.WebClient).DownloadString(`
	'https://raw.githubusercontent.com/MicrosoftDocs/Virtualization-Documentation/' + `
	'master/hyperv-tools/Convert-WindowsImage/Convert-WindowsImage.psm1'`
))

# Replace these values with the appropriate Windows Insider build number and ISO image file path
$previewBuild = '20150'
$insiderISO = "${env:HOMEDRIVE}${env:HOMEPATH}\Desktop\Windows10_InsiderPreview_Client_x64_en-us_${previewBuild}.iso"
$insiderVHDX = $insiderISO.Replace('.iso', '.vhdx')

# Create a VHDX image and populate it with the contents of the Windows Insider ISO image
Convert-WindowsImage `
	-SourcePath  $insiderISO `
	-VHDPath     $insiderVHDX `
	-VhdType     'Fixed' `
	-SizeBytes   80GB `
	-DiskLayout  'UEFI' `
	-Edition     'Windows 10 Pro' `
	-BCDinVHD    'NativeBoot' `
	-RemoteDesktopEnable

# Mount the created VHDX image and retrieve the drive letter of the mounted partition
$mounted = (Mount-VHD -Path $insiderVHDX -PassThru | Get-Disk | Get-Partition | Get-Volume).DriveLetter

# Create a boot entry for the VHDX image (this will automatically be set to the default boot entry)
bcdboot "${mounted}:\Windows"

# Set a human-readable description for the boot entry
bcdedit /set '{default}' DESCRIPTION "Windows Insider Build ${previewBuild}"

# Unmount the VHDX image
Dismount-VHD -Path $insiderVHDX

# Verify that the boot entry was configured correctly
bcdedit /enum
