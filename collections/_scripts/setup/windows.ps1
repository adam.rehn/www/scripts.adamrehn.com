# Don't terminate when a native command writes to stderr
$ErrorActionPreference = 'Continue';

# The full list of supported loadouts
$VALID_LOADOUTS = @('base', 'c++', 'go', 'perforce')


# Runs a command and terminates script execution if the exit code is nonzero
# (Adapted from here: <https://stackoverflow.com/a/58184408>)
function RUN()
{
	if ($args.Count -eq 0) {
		return
	}
	
	$command = $args[0]
	$commandArgs = @()
	if ($args.Count -gt 1) {
		$commandArgs = $args[1..($args.Count - 1)]
	}
	
	& $command $commandArgs
	if ($LASTEXITCODE -ne 0) {
		throw "The command '$command $commandArgs' terminated with exit code $LASTEXITCODE"
	}
}


# The user can specify one or more loadouts to install via command-line parameters
$loadouts = $args

# If no loadouts were specified then just enable the base loadout
if ($loadouts.Length -eq 0) {
	$loadouts = @('base')
}

# If "all" was specified then enable all loadouts
if ($loadouts -contains 'all') {
	$loadouts = $VALID_LOADOUTS
}

# Verify that each of the specified loadouts is valid
foreach ($loadout in $loadouts)
{
	if (-Not ($VALID_LOADOUTS -contains $loadout))
	{
		Write-Host "Error: unknown loadout '$loadout'!" -ForegroundColor Red
		exit 1
	}
}

# Determine if we are running Windows 10, Windows Server or Windows Server Core
$installationType = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion' -Name InstallationType).InstallationType
$isClient = $installationType.Equals('Client')
$isServer = -not $isClient
$isServerCore = $isServer -and $installationType.Contains('Core')
$hasGUI = $isClient -or (-not $isServerCore)

# Print the system details
Write-Host "Installation type:    $installationType"
Write-Host "Installation has GUI: $hasGUI"

# Print the list of enabled loadouts
Write-Host 'Enabled loadouts:'
foreach ($loadout in $loadouts) {
	Write-Host "- $loadout"
}

# Determine whether we are installing the base loadout
if ($loadouts -contains 'base')
{
	# Install Chocolatey
	Set-ExecutionPolicy Bypass -Scope Process -Force;
	Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
	
	# Import the `Update-SessionEnvironment` command and its `refreshenv` alias
	# (Adapted from here: <https://stackoverflow.com/a/46760714>)
	$chocolateyInstall = Convert-Path "$((Get-Command choco).Path)\..\.."
	Import-Module "$chocolateyInstall\helpers\chocolateyProfile.psm1"
	
	# Install 7-Zip, git and Python
	RUN choco install -y 7zip
	RUN choco install -y git --params '/NoAutoCrlf'
	RUN choco install -y python
	Update-SessionEnvironment
	
	# Install our Python packages
	RUN pip3 install `
		docker-shell `
		packaging `
		requests `
		setuptools `
		twine `
		wheel
	
	# If we are running in a graphical environment then install additional components
	if ($hasGUI)
	{
		# Install Firefox, Google Drive File Stream, the Sysinternals Suite and Visual Studio Code
		RUN choco install -y firefox google-drive-file-stream sysinternals
		RUN choco install -y vscode --params '/NoDesktopIcon'
		
		# Remove the Google Drive desktop icons, since the Chocolatey package doesn't provide an option to suppress their generation
		Remove-Item "$env:USERPROFILE\Desktop\*.lnk"
	}
	
	# Configure git
	RUN git config --global user.name 'Adam Rehn'
	RUN git config --global user.email 'adam@adamrehn.com'
	RUN git config --global core.autocrlf false
	
	# Install the appropriate Docker distribution depending on whether we are running Windows 10 or Windows Server
	# (Perform this step last, since it typically requires a reboot under Windows Server)
	if ($isClient)
	{
		# Install Docker Desktop
		RUN choco install -y docker-desktop
		
		# Remove the Docker Desktop desktop icon, since the Chocolatey package doesn't provide an option to suppress its generation
		Remove-Item "$env:USERPROFILE\Desktop\Docker Desktop.lnk"
	}
	else
	{
		# Install the NuGet package provider so we don't get prompted to do so when installing the Docker provider
		Install-PackageProvider -Name NuGet -Force
		
		# Install the Docker package provider and the Docker package
		Install-Module DockerMsftProvider -Force
		Install-Package Docker -ProviderName DockerMsftProvider -Force
	}
}

# Determine whether we are installing the C++ loadout
if ($loadouts -contains 'c++')
{
	# Install CMake
	RUN choco install -y cmake
	
	# Install the Visual C++ redistributable package
	RUN choco install -y vcredist-all
	
	# Install the Visual Studio Build Tools
	RUN choco install -y visualstudio2019buildtools visualstudio2019-workload-vctools
}

# Determine whether we are installing the Go loadout
if ($loadouts -contains 'go')
{
	# Install Go
	RUN choco install -y golang
}

# Determine whether we are installing the Perforce loadout
if ($loadouts -contains 'perforce')
{
	# Install P4V
	RUN choco install -y p4v
}
