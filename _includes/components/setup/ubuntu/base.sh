#!/usr/bin/env bash
set -e


# Retrieve the absolute path to this script
SCRIPT=`realpath "${BASH_SOURCE[0]}"`

# Determine whether the system has at least one NVIDIA graphics device
# (Make sure we always get an exit code of zero, otherwise script execution will halt)
NVIDIA_GPU=`lspci | grep NVIDIA || true`

# Helper function to add a package repository if we don't already have it
function AddAptSource {
	local source_entry="$1"
	local source_file="$2"
	if grep -q -F -e "$source_entry" "$source_file"; then
		echo "Source list file $source_file already contains the specified repository entry."
	else
		echo "Adding repository entry to source list file $source_file..."
		echo "$source_entry" >> "$source_file"
	fi
}

# Helper function to download an OpenPGP certificate if we don't already have it
function AddPGPCertificate {
	local cert_url="$1"
	local dest_file="$2"
	if [ -f "$dest_file" ]; then
		echo "OpenPGP certificate file $dest_file already exists, skipping download."
	else
		echo "Downloading OpenPGP certificate file from $cert_url to $dest_file..."
		curl -fsSL "$cert_url" | gpg --dearmor -o "$dest_file"
	fi
}

# Helper function to determine whether a given command exists
function CommandExists {
	command -v "$1" >/dev/null 2>&1
}


# Determine which of our three script phases has been selected
PHASE="$1"
if [ "$PHASE" == '' ]; then
	
	
	###
	### The first phase is the initial invocation of the script that runs the other two phases
	###
	
	# Ensure the script is not run as root, since the third phase runs commands as the login user
	if [ `id -u` -eq 0 ]; then
		echo "Error: this script must not be run as root!"
		exit 1
	fi
	
	# If the system has an NVIDIA GPU then verify that the NVIDIA binary drivers have been installed
	if [[ -n "$NVIDIA_GPU" ]]; then
		CommandExists nvidia-smi || {
			echo "Error: the system has an NVIDIA GPU but the NVIDIA binary drivers are not installed!"
			exit 1
		}
	fi
	
	{% comment -%} # If we have any custom steps for the first phase then include them {%- endcomment -%}
	{%- if include.phase1 -%}
	{{ include.phase1 }}
	{%- endif %}
	
	# Invoke the second phase, which runs commands that need to be run as root
	# (We pass the $USER environment variable as an argument since `sudo` modifies its value)
	echo 'Running root commands...'
	sudo bash "$SCRIPT" root "$USER"
	
	# Invoke the third phase, which runs commands that need to be run as the login user
	echo 'Running non-root commands...'
	bash "$SCRIPT" nonroot
	
	
elif [ "$PHASE" == 'root' ]; then
	
	
	###
	### The second phase runs commands that need to be run as root
	###
	
	
	# Retrieve the value of the $USER environment variable so we know who the login user is
	USER="$2"
	
	# Disable interactive prompts during package installation
	export DEBIAN_FRONTEND=noninteractive
	
	# Assemble the list of system packages to install
	SYSTEM_PACKAGES=(
		
		{% comment -%} # Include the common set of system packages {%- endcomment -%}
		{% include components/setup/ubuntu/common/packages-system.sh %}
		
		{%- comment -%} # If we have additional system packages for this specific script then include them {%- endcomment -%}
		{%- if include.packages_system -%}
		{{ include.packages_system }}
		{%- endif -%}
	)
	
	# Assemble the list of Python packages to install
	PYTHON_PACKAGES=(
		
		{% comment -%} # Include the common set of Python packages {%- endcomment -%}
		{% include components/setup/ubuntu/common/packages-python.sh %}
		
		{%- comment -%} # If we have additional Python packages for this specific script then include them {%- endcomment -%}
		{%- if include.packages_python -%}
		{{ include.packages_python }}
		{%- endif -%}
	)
	
	# Assemble the list of Ruby gems to install
	RUBY_GEMS=(
		
		{% comment -%} # Include the common set of Ruby packages {%- endcomment -%}
		{% include components/setup/ubuntu/common/packages-ruby.sh %}
		
		{%- comment -%} # If we have additional Ruby packages for this specific script then include them {%- endcomment -%}
		{%- if include.packages_ruby -%}
		{{ include.packages_ruby }}
		{%- endif -%}
	)
	
	# Print commands as they're executed
	set -x
	
	{% comment -%} # If we have any custom steps that take place prior to package installation then include them {%- endcomment -%}
	{% if include.before_packages -%}
	{{ include.before_packages }}
	{%- endif -%}
	
	# Ensure the universe repository is enabled
	add-apt-repository -y universe
	
	# Install our system packages
	apt-get update && apt-get install -y --allow-downgrades --no-install-recommends "${SYSTEM_PACKAGES[@]}"
	
	# Install our Python packages
	pip3 install "${PYTHON_PACKAGES[@]}"
	
	# Install our Ruby gems
	gem install "${RUBY_GEMS[@]}"
	
	{% comment -%} # Install common tools that require custom repository setup or are distributed via AppImage/Flatpak/etc. {%- endcomment -%}
	{% comment -%} include components/setup/ubuntu/common/install-cloud-tools.sh {%- endcomment -%}
	{% include components/setup/ubuntu/common/install-docker.sh %}
	{% include components/setup/ubuntu/common/install-imagemagick.sh %}
	
	{%- comment -%} # If we have any custom steps that take place after package installation then include them {%- endcomment -%}
	{%- if include.after_packages -%}
	{{ include.after_packages }}
	{%- endif -%}
	
	# Clean up any unneeded packages
	apt autoremove -y
	
	
elif [ "$PHASE" == 'nonroot' ]; then
	
	
	###
	### The third phase runs commands that need to be run as the login user
	###
	
	# Print commands as they're executed
	set -x
	
	# Alias `docker-compose` to `docker compose` for compatibility with older scripts that use Docker Compose
	ALIASES_FILE="$HOME/.bash_aliases"
	ALIAS_EXISTS=`cat "$ALIASES_FILE" 2>&1 | grep 'alias docker-compose' || true`
	if [[ -z "$ALIAS_EXISTS" ]]; then
		echo 'alias docker-compose="docker compose"' >> "$ALIASES_FILE"
	fi
	
	# Install Node.js for the current user using NVM
	{% include components/setup/ubuntu/common/install-nodejs.sh %}
	
	# Apply our common configuration options for git
	{% include components/setup/ubuntu/common/configure-git.sh %}
	
	{%- comment -%} # If we have any custom steps for the third phase then include them {%- endcomment -%} 
	{%- if include.phase3 -%}
	{{ include.phase3 }}
	{%- endif -%}
	
	# Inform the user that installation is complete and that a logout or restart is required
	set +x
	echo 'Installation is complete! Please restart the machine (or just log out and log'
	echo 'back in) to ensure Docker commands are able to run without requiring `sudo`.'
	
else
	
	echo "Error: invalid invocation! Please run this script without any parameters."
	exit 1
	
fi
