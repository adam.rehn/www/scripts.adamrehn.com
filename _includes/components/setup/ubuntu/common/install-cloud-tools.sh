## Download the Google Cloud public signing key if we don't already have it
#AddPGPCertificate 'https://packages.cloud.google.com/apt/doc/apt-key.gpg' /usr/share/keyrings/cloud.google.gpg

## Install kubectl if it is not already installed
#CommandExists kubectl || {
#	
#	# Add the Kubernetes public signing key and package repository
#	AddPGPCertificate 'https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key' /etc/apt/keyrings/kubernetes-apt-keyring.gpg
#	AddAptSource 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' /etc/apt/sources.list.d/kubernetes.list
#	
#	# Install kubectl
#	apt-get update && apt-get install -y --no-install-recommends kubectl
#}

## Install the AWS CLI if it is not already installed
#CommandExists aws || {
#	
#	# Download and run the AWS CLI installer
#	curl -fsSL 'https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip' -o /tmp/awscliv2.zip
#	unzip /tmp/awscliv2.zip -d /tmp
#	/tmp/aws/install
#	
#	# Perform cleanup
#	rm -rf /tmp/aws
#	rm /tmp/awscliv2.zip
#}

## Install eksctl if it is not already installed
#CommandExists eksctl || {
#	curl -fsSL "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
#	mv /tmp/eksctl /usr/local/bin
#	chmod +x /usr/local/bin/eksctl
#}

## Install the Azure CLI if it is not already installed
#CommandExists az || {
#	
#	# Add the Microsoft Azure public signing key and package repository
#	AddPGPCertificate 'https://packages.microsoft.com/keys/microsoft.asc' /etc/apt/keyrings/microsoft.gpg
#	AddAptSource "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/microsoft.gpg] https://packages.microsoft.com/repos/azure-cli/ $(lsb_release -cs) main" /etc/apt/sources.list.d/azure-cli.list
#	
#	# Install the Azure CLI
#	apt-get update && apt-get install -y --no-install-recommends azure-cli
#}

## Install the Google Cloud CLI if it is not already installed
#CommandExists gcloud || {
#	
#	# Add the Google Cloud package repository
#	AddAptSource 'deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main' /etc/apt/sources.list.d/google-cloud-sdk.list
#	
#	# Install the Google Cloud SDK
#	apt-get update && apt-get install -y --no-install-recommends google-cloud-cli
#}

## Install HashiCorp Packer and Terraform if they aren't already installed
#CommandExists packer || {
#	
#	# Add the HashiCorp public signing key and package repository
#	AddPGPCertificate 'https://apt.releases.hashicorp.com/gpg' /usr/share/keyrings/hashicorp-archive-keyring.gpg
#	AddAptSource "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" /etc/apt/sources.list.d/hashicorp.list
#	
#	# Install Packer and Terraform
#	apt-get update && apt-get install -y --no-install-recommends \
#		packer \
#		terraform
#}
