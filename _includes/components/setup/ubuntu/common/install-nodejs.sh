# Install Node.js for the current user if it is not already installed
CommandExists nvm || {
	
	# Install the Node Version Manager (NVM)
	curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
	
	# Use NVM to install the latest LTS release of Node.js
	source "$HOME/.nvm/nvm.sh"
	nvm install --lts
	nvm use --lts
	
}
