# Disable automatic newline conversion when working with git repositories
git config --global core.autocrlf false
