# Install ImageMagick if it is not already installed
CommandExists magick || {
	
	# Download the latest AppImage release of ImageMagick
	curl -fsSL 'https://imagemagick.org/archive/binaries/magick' -o /usr/local/bin/magick
	chmod +x /usr/local/bin/magick
	
}
