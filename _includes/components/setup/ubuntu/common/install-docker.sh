# Install Docker if it is not already installed
CommandExists docker || {
	
	# Install Docker
	AddPGPCertificate 'https://download.docker.com/linux/ubuntu/gpg' /etc/apt/keyrings/docker.gpg
	AddAptSource "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" /etc/apt/sources.list.d/docker.list
	apt-get update && apt-get install -y --no-install-recommends docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
	
	# Perform Docker post-installation configuration
	usermod -aG docker "$USER"
	
}

# If the system has an NVIDIA GPU, and the NVIDIA Container Toolkit is not already installed, then install it
if [[ -n "$NVIDIA_GPU" ]]; then
	CommandExists nvidia-ctk || {
		
		# Note: packages are only available for LTS releases, so we use the 22.04 packages
		DISTRIBUTION='ubuntu22.04'
		REAL_DISTRIBUTION=$(. /etc/os-release;echo $ID$VERSION_ID)
		echo "Distribution is $REAL_DISTRIBUTION, installing NVIDIA Container Toolkit packages for $DISTRIBUTION..."
		AddPGPCertificate 'https://nvidia.github.io/libnvidia-container/gpgkey' /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg
		SOURCES_LIST=`curl -s -L "https://nvidia.github.io/libnvidia-container/$DISTRIBUTION/libnvidia-container.list" | sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g'`
		AddAptSource "$SOURCES_LIST" /etc/apt/sources.list.d/nvidia-container-toolkit.list
		apt-get update && apt-get install -y --no-install-recommends nvidia-container-toolkit
		
	}
fi
