{%- capture packages_system -%}
	
	{% include components/setup/ubuntu/adam/packages-system.sh %}
	
	{% if include.packages_system %}
	{{ include.packages_system }}
	{% endif %}
	
{%- endcapture -%}

{%- capture phase3 -%}
	
	{% include components/setup/ubuntu/adam/favourites.sh %}
	{% include components/setup/ubuntu/adam/theme.sh %}
	{% include components/setup/ubuntu/adam/terminal-profile.sh %}
	{% include components/setup/ubuntu/adam/firefox-tweaks.sh %}
	{% include components/setup/ubuntu/adam/shell-extensions.sh %}
	
	{% if include.phase3 %}
	{{ include.phase3 }}
	{% endif %}
	
{%- endcapture -%}

{%-
	include components/setup/ubuntu/desktop.sh
	phase1 = include.phase1
	packages_system = packages_system
	packages_python = include.packages_python
	packages_ruby = include.packages_ruby
	before_packages = include.before_packages
	after_packages = include.after_packages
	phase3 = phase3
-%}
