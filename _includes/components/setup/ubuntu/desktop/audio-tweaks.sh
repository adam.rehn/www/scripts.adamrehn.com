# Remove the "speech-dispatcher" speech synthesis daemon to avoid hitting the bug that distorts all system audio
# (See: <https://bugs.launchpad.net/ubuntu/+source/speech-dispatcher/+bug/1736222>)
apt remove -y speech-dispatcher && apt autoremove -y

# Rename the shortcut for PulseAudio Volume Control to "Volume Mixer" to make it easier to search for
sed -i 's|Name=PulseAudio Volume Control|Name=Volume Mixer|' /usr/share/applications/pavucontrol.desktop
sed -i 's|GenericName=Volume Control|GenericName=Volume Mixer|' /usr/share/applications/pavucontrol.desktop
