# Build the libsecret credential store that ships with git in source form
pushd /usr/share/doc/git/contrib/credential/libsecret
make
popd

# Download the D-Bus Secret Service credential store for Docker if we don't already have it
SECRETSERVICE_BINARY=/usr/local/bin/docker-credential-secretservice
SECRETSERVICE_URL='https://github.com/docker/docker-credential-helpers/releases/download/v0.7.0/docker-credential-secretservice-v0.7.0.linux-amd64'
test -f "$SECRETSERVICE_BINARY" || {
	curl -fL "$SECRETSERVICE_URL" -o "$SECRETSERVICE_BINARY"
	chmod +x "$SECRETSERVICE_BINARY"
}
