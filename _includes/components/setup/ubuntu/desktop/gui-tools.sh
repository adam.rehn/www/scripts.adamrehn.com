# Install Postman if it is not already installed
snap install postman --channel=v10/stable

# Install Google Chrome if it is not already installed
CommandExists google-chrome || {
	curl -fL 'https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb' -o /tmp/chrome.deb
	apt install -y /tmp/chrome.deb
	rm /tmp/chrome.deb
}

# Install Discord if it is not already installed
CommandExists discord || {
	curl -fL 'https://discord.com/api/download?platform=linux&format=deb' -o /tmp/discord.deb
	apt install -y /tmp/discord.deb
	rm /tmp/discord.deb
}

# Install draw.io (now known as diagrams.net) if it is not already installed
CommandExists drawio || {
	curl -fL 'https://github.com/jgraph/drawio-desktop/releases/download/v24.4.13/drawio-amd64-24.4.13.deb' -o /tmp/drawio.deb
	apt install -y /tmp/drawio.deb
	rm /tmp/drawio.deb
}

# Install Insync if it is not already installed
CommandExists insync || {
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ACCAF35C
	add-apt-repository -y "deb [arch=amd64] http://apt.insync.io/ubuntu $(lsb_release -cs) non-free contrib"
	apt-get update && apt-get install -y --no-install-recommends insync insync-nautilus
}

# Install OBS if it is not already installed
CommandExists obs || {
	add-apt-repository -y ppa:obsproject/obs-studio
	apt-get update && apt-get install -y --no-install-recommends obs-studio
}

# Install Slack if it is not already installed
CommandExists slack || {
	SLACK_DEB_LINK=`curl -fL 'https://slack.com/intl/en-au/downloads/instructions/linux?ddl=1&build=deb' | python3 -c 'from bs4 import BeautifulSoup; import sys; print(BeautifulSoup(sys.stdin.read()).findAll("a", href=True, text="Try again.")[0]["href"])'`
	curl -fL "$SLACK_DEB_LINK" -o /tmp/slack.deb
	apt install -y /tmp/slack.deb
	rm /tmp/slack.deb
}

# Install VirtualBox if it is not already installed
CommandExists virtualbox || {
	
	# Add the VirtualBox public signing key and package repository
	AddPGPCertificate 'https://www.virtualbox.org/download/oracle_vbox_2016.asc' /usr/share/keyrings/oracle-virtualbox-2016.gpg
	AddAptSource "deb [arch=amd64 signed-by=/usr/share/keyrings/oracle-virtualbox-2016.gpg] https://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib" /etc/apt/sources.list.d/virtualbox.list
	
	# Install VirtualBox
	apt-get update && apt-get install -y --no-install-recommends virtualbox-7.0
	
}

# Install Visual Studio Code if it is not already installed
CommandExists code || {
	curl -fL 'https://go.microsoft.com/fwlink/?LinkID=760868' -o /tmp/vscode.deb
	apt install -y /tmp/vscode.deb
	rm /tmp/vscode.deb
}

# Install Zoom if it is not already installed
CommandExists zoom || {
	curl -fL 'https://zoom.us/client/latest/zoom_amd64.deb' -o /tmp/zoom.deb
	apt install -y /tmp/zoom.deb
	rm /tmp/zoom.deb
}

# Install klogg if it is not already installed
CommandExists klogg || {
	AddPGPCertificate 'https://klogg.filimonov.dev/klogg.gpg.key' /etc/apt/keyrings/klogg.gpg
	curl -fL 'https://klogg.filimonov.dev/deb/klogg.jammy.list' | tee /etc/apt/sources.list.d/klogg.list
	apt-get update && apt-get install -y --no-install-recommends klogg
}

# Install Flatseal if it is not already installed
flatpak install -y --noninteractive flathub 'com.github.tchx84.Flatseal'

# Install OnlyOffice if it is not already installed
flatpak install -y --noninteractive flathub 'org.onlyoffice.desktopeditors'

# Disable network access for OnlyOffice to prevent telemetry submission
flatpak override --system --unshare=network 'org.onlyoffice.desktopeditors'
