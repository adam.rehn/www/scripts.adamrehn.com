# The deb version of Firefox
firefox

# Flatpak support
flatpak

# libsecret development libraries needed to build the libsecret credential store that ships with git
libsecret-1-0
libsecret-1-dev

# GUI tools for system administration
baobab
chrome-gnome-shell
gnome-tweaks
gnome-shell-extension-prefs
gnome-system-monitor
gparted
grub-customizer
pavucontrol
remmina
remmina-plugin-rdp
ssh-askpass-gnome
virt-manager

# Enables SPICE support when viewing VMs through virt-manager
gir1.2-spiceclientgtk-3.0

# GUI tools that could potentially run in a container, but are run on the host system for convenience
gimp
inkscape
vlc

# OpenConnect VPN client with system network manager integration
openconnect
network-manager-openconnect
network-manager-openconnect-gnome
