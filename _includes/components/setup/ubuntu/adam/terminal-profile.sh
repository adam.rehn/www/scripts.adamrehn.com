# Retrieve the UUID of the default Gnome Terminal profile to construct the gsettings schema and path
TERMINAL_PROFILE=$(gsettings get org.gnome.Terminal.ProfilesList default | tr -d \')


# Configures a setting for the default Gnome Terminal profile
function ConfigureTerminalProfile {
	gsettings set "org.gnome.Terminal.Legacy.Profile:/org/gnome/terminal/legacy/profiles:/:${TERMINAL_PROFILE}/" "$1" "$2"
}


# Configure the default profile to use a purple background instead of the colour specified by the GTK theme
ConfigureTerminalProfile use-theme-colors false
ConfigureTerminalProfile foreground-color 'rgb(255, 255, 255)'
ConfigureTerminalProfile background-color 'rgb(48, 10, 36)'

# Configure the default profile to display bold text in a brighter colour
ConfigureTerminalProfile bold-is-bright true
