# Download the update-favourites Python script from the Linux QoL repository
curl -fsSL 'https://raw.githubusercontent.com/adamrehn/qol-linux/main/tools/update-favourites/update-favourites.py' -o /tmp/update-favourites.py

# Add Firefox, Terminal, VS Code and draw.io to the GNOME Shell favourites list, and remove the Software Store and Help entries
python3 /tmp/update-favourites.py \
	--add 'firefox.desktop' 'org.gnome.Terminal.desktop' 'code.desktop' 'drawio.desktop' \
	--remove 'snap-store_ubuntu-software.desktop' 'yelp.desktop'

# Perform cleanup
rm /tmp/update-favourites.py
