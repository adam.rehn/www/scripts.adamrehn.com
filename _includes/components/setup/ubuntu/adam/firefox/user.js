// Enable loading CSS rules from userChrome.css
user_pref("toolkit.legacyUserProfileCustomizations.stylesheets", true);

// Block desktop notification requests from all websites
user_pref("permissions.default.desktop-notification", 2);

// Hide the title bar
user_pref("browser.tabs.inTitlebar", 1);
