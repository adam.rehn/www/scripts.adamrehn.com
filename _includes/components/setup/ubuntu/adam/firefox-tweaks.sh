# Clone the Firefox automation tools repository
git clone --depth=1 'https://gitlab.com/adam.rehn/qol/firefox-automation.git' /tmp/firefox-automation

# Start Firefox at least once to ensure a default profile has been created
set +x
echo 'Performing Firefox first run to create the default profile...'
firefox --headless --MOZ_LOG_FILE=/dev/null 2>&1 >/dev/null &
sleep 10
killall --wait firefox
echo 'Firefox first run complete. Ignore any error messages above.'
set -x

# Retrieve the absolute filesystem path for the default profile
FIREFOX_PROFILE=`python3 /tmp/firefox-automation/tools/list-profiles.py --default --paths-only`

# Copy our custom `user.js` file to the default Firefox profile
cat << EOF > "$FIREFOX_PROFILE/user.js"
{% include components/setup/ubuntu/adam/firefox/user.js %}
EOF

# Copy our custom `userChrome.css` file to the default Firefox profile
FIREFOX_CHROME="$FIREFOX_PROFILE/chrome"
mkdir -p "$FIREFOX_CHROME"
cat << EOF > "$FIREFOX_CHROME/userChrome.css"
{% include components/setup/ubuntu/adam/firefox/userChrome.css %}
EOF

# Perform cleanup
rm -rf /tmp/firefox-automation
