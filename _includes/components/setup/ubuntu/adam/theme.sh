# Setup the Fluent GTK theme if it is not already installed
if [ ! -d "$HOME/.themes/Fluent-orange" ]; then
	
	# Download the source code for the Fluent GTK theme
	git clone --progress --depth=1 'https://github.com/vinceliuice/Fluent-gtk-theme.git' /tmp/fluent-gtk-theme
	pushd /tmp/fluent-gtk-theme
	
	# Tweak the colour of the close window button
	sed -i 's|red-600: #E53935|red-600: #E81123|' src/_sass/_color-palette.scss
	sed -i 's|red-300|red-600|' src/_sass/gtk/_common-3.20.scss
	sed -i 's|red-300|red-600|' src/_sass/gtk/_common-4.0.scss
	
	# Remove the slight window edge rounding from the non-round version of the theme
	sed -i 's|, 8px, 3px|, 8px, 0px|' src/_sass/_variables.scss
	
	# Install the theme and enable it
	./parse-sass.sh
	./install.sh --tweaks solid --tweaks square --theme orange
	gsettings set org.gnome.desktop.interface gtk-theme 'Fluent-orange'
	
	# Remove the source code for the theme
	popd
	rm -rf /tmp/fluent-gtk-theme
	
fi

# Set the GNOME Shell theme to the dark version of Yaru
# (Note that this only affects shell overlays, GTK applications will still use Fluent)
dconf write /org/gnome/shell/extensions/user-theme/name "'Yaru-dark'"

# Set the cursor theme to DMZ White
gsettings set org.gnome.desktop.interface cursor-theme 'DMZ-White'
