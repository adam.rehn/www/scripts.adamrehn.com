# Installs a GNOME shell extension from the extensions website if it is not already installed
# The first and only argument specifies the UUID of the extension to install
function InstallShellExtension {
	if [ ! -d "$HOME/.local/share/gnome-shell/extensions/$1" ]; then
		
		# Send a synchrounous installation request, and set the timeout to DBUS_TIMEOUT_INFINITE (0x7fffffff)
		dbus-send \
			--session \
			--print-reply \
			--dest=org.gnome.Shell \
			--reply-timeout=2147483647 \
			/org/gnome/Shell \
			org.gnome.Shell.Extensions.InstallRemoteExtension \
			string:"$1"
		
	fi
}

# Configures a setting for the Colour Picker shell extension using gsettings
function ConfigureColourPicker {
	gsettings --schemadir "$HOME/.local/share/gnome-shell/extensions/color-picker@tuberry/schemas" set org.gnome.shell.extensions.color-picker "$1" "$2"
}

# Configures a setting for the Dash To Panel shell extension using gsettings
function ConfigureDashToPanel {
	gsettings --schemadir "$HOME/.local/share/gnome-shell/extensions/dash-to-panel@jderose9.github.com/schemas" set org.gnome.shell.extensions.dash-to-panel "$1" "$2"
}

# Configures a setting for the Just Perfection shell extension using gsettings
function ConfigureJustPerfection {
	gsettings --schemadir "$HOME/.local/share/gnome-shell/extensions/just-perfection-desktop@just-perfection/schemas" set org.gnome.shell.extensions.just-perfection "$1" "$2"
}

# Configures a setting for the System Monitor shell extension using gsettings
function ConfigureSystemMonitor {
	gsettings --schemadir "$HOME/.local/share/gnome-shell/extensions/system-monitor@paradoxxx.zero.gmail.com/schemas" set org.gnome.shell.extensions.system-monitor "$1" "$2"
}

# Disable version validation so Gnome 40 extensions will be loaded under Gnome 42
gsettings set org.gnome.shell 'disable-extension-version-validation' true

# Install the Backslide shell extension
InstallShellExtension 'backslide@codeisland.org'

# Install the Colour Picker shell extension
InstallShellExtension 'color-picker@tuberry'

# Install the Dash To Panel shell extension
InstallShellExtension 'dash-to-panel@jderose9.github.com'

# Install the gTile shell extension
InstallShellExtension 'gTile@vibou'

# Install the Just Perfection shell extension
InstallShellExtension 'just-perfection-desktop@just-perfection'

# Install the MPRIS Indicator Button shell extension
InstallShellExtension 'mprisindicatorbutton@JasonLG1979.github.io'

# Install the No Overview At Start-up shell extension
InstallShellExtension 'no-overview@fthx'

# Install the Sound Input & Output Device Chooser shell extension
InstallShellExtension 'sound-output-device-chooser@kgshank.net'

# Install the System Monitor shell extension
InstallShellExtension 'system-monitor@paradoxxx.zero.gmail.com'

# Install the User Themes shell extension
InstallShellExtension 'user-theme@gnome-shell-extensions.gcampax.github.com'

# Configure the Colour Picker shell extension to use the custom eyedropper icon from older versions of the extension
ICONS_DIR="$HOME/Pictures/Icons"
EYEDROPPER_ICON="$ICONS_DIR/dropper-symbolic.svg"
if [ ! -f "$EYEDROPPER_ICON" ]; then
	mkdir -p "$ICONS_DIR"
	curl -fsSL 'https://raw.githubusercontent.com/tuberry/color-picker/42/color-picker%40tuberry/icons/dropper-symbolic.svg' -o "$EYEDROPPER_ICON"
	ConfigureColourPicker systray-dropper-icon "'$EYEDROPPER_ICON'"
fi

# Configure the position and layout of the Dash To Panel shell extension
ConfigureDashToPanel primary-monitor 0
ConfigureDashToPanel panel-lengths '{"0":100}'
ConfigureDashToPanel panel-position 'BOTTOM'
ConfigureDashToPanel panel-positions '{"0":"BOTTOM"}'
ConfigureDashToPanel panel-element-positions '{"0":[{"element":"showAppsButton","visible":true,"position":"stackedTL"},{"element":"activitiesButton","visible":false,"position":"stackedTL"},{"element":"leftBox","visible":true,"position":"stackedTL"},{"element":"taskbar","visible":true,"position":"stackedTL"},{"element":"centerBox","visible":true,"position":"stackedBR"},{"element":"rightBox","visible":true,"position":"stackedBR"},{"element":"dateMenu","visible":true,"position":"stackedBR"},{"element":"systemMenu","visible":true,"position":"stackedBR"},{"element":"desktopButton","visible":true,"position":"stackedBR"}]}'

# Configure the appearance of the Dash To Panel shell extension
_DASH_TO_PANEL_DOT_COLOUR='#5294e2'
ConfigureDashToPanel appicon-margin 8
ConfigureDashToPanel appicon-padding 6
ConfigureDashToPanel dot-size 1
ConfigureDashToPanel dot-position 'BOTTOM'
ConfigureDashToPanel dot-style-focused 'METRO'
ConfigureDashToPanel dot-style-unfocused 'METRO'
ConfigureDashToPanel dot-color-dominant false
ConfigureDashToPanel dot-color-override false
ConfigureDashToPanel dot-color-1 "$_DASH_TO_PANEL_DOT_COLOUR"
ConfigureDashToPanel dot-color-2 "$_DASH_TO_PANEL_DOT_COLOUR"
ConfigureDashToPanel dot-color-3 "$_DASH_TO_PANEL_DOT_COLOUR"
ConfigureDashToPanel dot-color-4 "$_DASH_TO_PANEL_DOT_COLOUR"
ConfigureDashToPanel dot-color-unfocused-1 "$_DASH_TO_PANEL_DOT_COLOUR"
ConfigureDashToPanel dot-color-unfocused-2 "$_DASH_TO_PANEL_DOT_COLOUR"
ConfigureDashToPanel dot-color-unfocused-3 "$_DASH_TO_PANEL_DOT_COLOUR"
ConfigureDashToPanel dot-color-unfocused-4 "$_DASH_TO_PANEL_DOT_COLOUR"
ConfigureDashToPanel dot-color-unfocused-different false
ConfigureDashToPanel focus-highlight true
ConfigureDashToPanel focus-highlight-dominant false
ConfigureDashToPanel focus-highlight-opacity 20
ConfigureDashToPanel panel-size 40
ConfigureDashToPanel panel-sizes '{}'
ConfigureDashToPanel trans-bg-color '#262524'
ConfigureDashToPanel trans-use-custom-bg true
ConfigureDashToPanel trans-use-custom-opacity true
ConfigureDashToPanel trans-panel-opacity 1.0

# Configure the behaviour of the Dash To Panel shell extension
ConfigureDashToPanel animate-app-switch true
ConfigureDashToPanel animate-window-launch true
ConfigureDashToPanel click-action 'CYCLE-MIN'
ConfigureDashToPanel group-apps false
ConfigureDashToPanel group-apps-use-fixed-width true
ConfigureDashToPanel hot-keys false
ConfigureDashToPanel intellihide false
ConfigureDashToPanel isolate-monitors false
ConfigureDashToPanel isolate-workspaces false
ConfigureDashToPanel progress-show-bar true
ConfigureDashToPanel progress-show-count true
ConfigureDashToPanel scroll-icon-action 'NOTHING'
ConfigureDashToPanel scroll-panel-action 'NOTHING'
ConfigureDashToPanel show-favorites true
ConfigureDashToPanel show-running-apps true
ConfigureDashToPanel show-tooltip true
ConfigureDashToPanel show-window-previews true

# Configure the Dash To Panel shell extension's "show desktop" button
ConfigureDashToPanel show-showdesktop-hover true
ConfigureDashToPanel show-showdesktop-time 300
ConfigureDashToPanel show-showdesktop-delay 1000
ConfigureDashToPanel showdesktop-button-width 16

# Configure the the Just Perfection shell extension to disable "window is ready" notifications
# and display all notifications at the bottom-right of the screen instead of the top-middle
ConfigureJustPerfection notification-banner-position 5
ConfigureJustPerfection window-demands-attention-focus true

# Configure the appearance of the System Monitor shell extension
ConfigureSystemMonitor background '#ffffff16'
ConfigureSystemMonitor icon-display false
ConfigureSystemMonitor show-tooltip true

# Configure the System Monitor shell extension to display CPU usage
ConfigureSystemMonitor cpu-display true
ConfigureSystemMonitor cpu-show-text true
ConfigureSystemMonitor cpu-show-menu true
ConfigureSystemMonitor cpu-graph-width 20

# Configure the System Monitor shell extension to display network I/O
ConfigureSystemMonitor net-display true
ConfigureSystemMonitor net-show-text true
ConfigureSystemMonitor net-show-menu true
ConfigureSystemMonitor net-graph-width 20

# Configure the System Monitor shell extension to display disk I/O
ConfigureSystemMonitor disk-display true
ConfigureSystemMonitor disk-show-text true
ConfigureSystemMonitor disk-show-menu true
ConfigureSystemMonitor disk-graph-width 20
ConfigureSystemMonitor disk-usage-style 'bar'

# Configure the System Monitor shell extension to hide all other resources (but show swap usage in the menu)
ConfigureSystemMonitor battery-display false
ConfigureSystemMonitor battery-show-menu false
ConfigureSystemMonitor fan-display false
ConfigureSystemMonitor fan-show-menu false
ConfigureSystemMonitor freq-display false
ConfigureSystemMonitor freq-show-menu false
ConfigureSystemMonitor gpu-display false
ConfigureSystemMonitor gpu-show-menu false
ConfigureSystemMonitor memory-display false
ConfigureSystemMonitor swap-display false
ConfigureSystemMonitor swap-show-menu true
ConfigureSystemMonitor thermal-display false
ConfigureSystemMonitor thermal-show-menu false
