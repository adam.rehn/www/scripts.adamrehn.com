# Dependencies for the System Monitor shell extension
gir1.2-gtop-2.0
gir1.2-nm-1.0
gir1.2-clutter-1.0

# Dependencies for the Fluent GTK theme
gnome-themes-extra
gtk2-engines-murrine
