{%- capture phase1 -%}
	
	# Disable screen blanking to ensure the session doesn't lock itself during the setup process
	gsettings set org.gnome.desktop.session 'idle-delay' 0
	
	{% if include.phase1 %}
	{{ include.phase1 }}
	{% endif %}
	
{%- endcapture -%}


{%- capture packages_system -%}
	
	{% include components/setup/ubuntu/desktop/packages-system.sh %}
	
	{% if include.packages_system %}
	{{ include.packages_system }}
	{% endif %}
	
{%- endcapture -%}


{%- capture before_packages -%}
	
	# Determine whether we are currently using the snap version of Firefox
	FIREFOX_SNAP=`snap aliases firefox 2>&1 | grep firefox.geckodriver || true`
	if [[ -n "$FIREFOX_SNAP" ]]; then
		
		# Close Firefox if it is currently running
		killall --wait --quiet firefox || true
		
		# Remove the snap version of Firefox
		snap remove --purge firefox
		
		# Add the PPA for the deb version of Firefox
		add-apt-repository -y 'ppa:mozillateam/ppa'
		
		# Force the use of the deb from the PPA, rather than the system deb that just installs the snap version
		# (From: <https://www.omgubuntu.co.uk/2022/04/how-to-install-firefox-deb-apt-ubuntu-22-04>)
		echo -e "Package: *\nPin: release o=LP-PPA-mozillateam\nPin-Priority: 1001" | tee /etc/apt/preferences.d/mozilla-firefox
		echo 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox
		
	fi
	
	# Add the PPA for Grub Customizer (if we don't already have it), which addresses the bug that resulted in its removal from the 22.04 repositories:
	# <https://bugs.launchpad.net/ubuntu/+source/grub-customizer/+bug/1969353>
	CommandExists grub-customizer || {
		add-apt-repository -y 'ppa:danielrichter2007/grub-customizer'
	}
	
	{% if include.before_packages %}
	{{ include.before_packages }}
	{% endif %}
	
{%- endcapture -%}


{%- capture after_packages -%}
	
	# Add Flathub to the list of Flatpak remotes
	flatpak remote-add --if-not-exists flathub 'https://dl.flathub.org/repo/flathub.flatpakrepo'
	
	{% include components/setup/ubuntu/desktop/audio-tweaks.sh %}
	{% include components/setup/ubuntu/desktop/credential-storage.sh %}
	{% include components/setup/ubuntu/desktop/gui-tools.sh %}
	
	{% if include.after_packages %}
	{{ include.after_packages }}
	{% endif %}
	
{%- endcapture -%}


{%- capture phase3 -%}
	
	# Configure the system to use UTC time and assume Windows has been configured with the corresponding registry key when dual-booting
	timedatectl set-local-rtc 0
	
	# Set the time display format to 12-hour time (AM/PM)
	gsettings set org.gnome.desktop.interface clock-format '12h'
	
	# Set the window action key to Alt so we can drag windows around using Alt-Click
	gsettings set org.gnome.desktop.wm.preferences mouse-button-modifier '<Alt>'
	
	# Show hidden files and directories in Nautilus by default
	gsettings set org.gtk.Settings.FileChooser show-hidden true
	
	# Configure git to use the libsecret credential store
	git config --global credential.helper /usr/share/doc/git/contrib/credential/libsecret/git-credential-libsecret
	
	{% if include.phase3 %}
	{{ include.phase3 }}
	{% endif %}
	
{%- endcapture -%}


{%-
	include components/setup/ubuntu/base.sh
	phase1 = phase1
	packages_system = packages_system
	packages_python = include.packages_python
	packages_ruby = include.packages_ruby
	before_packages = before_packages
	after_packages = after_packages
	phase3 = phase3
-%}
