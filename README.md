# Adam Rehn's Scripts

This is the repository for the *Adam Rehn's Scripts* website, live at https://scripts.adamrehn.com.


## Building the site locally

In order to generate the static site files, you will need [Ruby](https://www.ruby-lang.org/) and [Bundler](https://bundler.io/):

```bash
# Install the site's dependencies
bundle install

# Generate the site files and serve them locally
bundle exec jekyll serve
```


## Legal

Copyright &copy; 2020-2022, Adam Rehn. Licensed under the MIT License, see the file [LICENSE](./LICENSE) for details.
